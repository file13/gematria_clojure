(ns gematria.core-test
  (:require [clojure.test :refer :all]
            [gematria.core :refer :all]
            [clojure.string :as str]
            ))

(deftest test-digital-root
  (is (= (digital-root 0) 0))
  (is (= (digital-root 5) 5))
  (is (= (digital-root 9) 9))
  (is (= (digital-root 10) 1))
  (is (= (digital-root 17) 8))
  (is (= (map digital-root (range 1 100))
         (map digital-root (range 1 100))))
  (let [roots (vector 0 1 2 3 4 5 6 7 8 9)]
    (dotimes [i 10]
      (is (= (digital-root i) (roots i))))
    (dotimes [i 10]
      (let [r (digital-root (+ i 10))]
        (is (= r (roots r)))))
    (dotimes [i 10]
      (let [r (digital-root (+ i 920))]
        (is (= r (roots r)))))))

(def english-alphabet "abcdefghijklmnopqrstuvwxyz")

(deftest test-english-gematria-basic
  (is (= 1  (english-gematria-basic \a)))
  (is (= 1  (english-gematria-basic \A)))
  (is (= 26 (english-gematria-basic \Z)))
  (is (= 26 (english-gematria-basic \z)))
  (is (= 0  (english-gematria-basic \*))) ; test symbol
  (is (= 0  (english-gematria-basic \space)))
  (is (= 0  (english-gematria-basic \9)))
  (dotimes [i 26]
    (is (= (english-gematria-basic (nth english-alphabet i))
           (+ i 1))))
  (let [up-alphabet (str/upper-case english-alphabet)]
    (dotimes [i 26]
      (is (= (english-gematria-basic (nth up-alphabet i))
             (+ i 1)))))
  (let [numbers "0123456789"] ; as strings due to type checking
    (dotimes [i 10]
      (is (= (english-gematria-basic (nth numbers i)) 0))))
  (let [symbols "!$%^-_*"]
    (dotimes [i 7]
      (is (= (english-gematria-basic (nth symbols i)) 0)))))

(deftest test-gematria
  (let [troll "Archie"
        troll-sg (list 1 90 3 8 9 5)
        rat "Samantha"]
    (is (= 44   (gematria english-gematria-basic troll)))
    (is (= 116  (gematria english-gematria-standard troll)))
    (is (= 77   (gematria english-gematria-basic rat)))
    (is (= 401  (gematria english-gematria-standard rat)))
    (is (= 351  (gematria english-gematria-basic english-alphabet)))
    (is (= 4095 (gematria #'english-gematria-standard english-alphabet)))
    (is (= (list 1 18 3 8 9 5)
           (gematria english-gematria-basic troll :as-list true)))
    (is (= troll-sg
           (gematria english-gematria-standard troll :as-list true)))
    (is (= [116 troll-sg]
           (gematria english-gematria-standard troll :as-both true)))
    (is (= (list 1 2 3 4 5 6 7 8 9 10 
                 11 12 13 14 15 16 17 18 19 
                 20 21 22 23 24 25 26)
           (gematria english-gematria-basic english-alphabet :as-list true)))
    (is (= (list 1 2 3 4 5 6 7 8 9 
                 10 20 30 40 50 60 70 80 90 
                 100 200 300 400 500 600 700 800)
           (gematria
            english-gematria-standard english-alphabet :as-list true)))))

(deftest test-english-char?
  (is (= (english-char? \a) true))
  (is (= (english-char? \Z) true))
  (is (= (english-char? \ΰ) false)))

(deftest test-normalize-english-char
  (is (= (normalize-english-char \ΰ) \υ))
  (is (= (normalize-english-char \E) \e))
  (is (= (normalize-english-char \É) \e)))

(deftest test-normalize-english
  (is (= (normalize-english "Archie") "archie"))
  (is (= (normalize-english "Égalité") "egalite")))

(deftest test-english-basic-of-string
  (is (= (english-basic-of-string "a") [1 '(1)]))
  (is (= (english-basic-of-string "Archie") [44 '(1 18 3 8 9 5)]))
  (is (= (english-basic-of-string "aR!_ chi$E") [44 '(1 18 0 0 0 3 8 9 0 5)]))
  (is (= (english-basic-of-string "Archie1")) '(45 (1 18 3 8 9 5 1)))
  (is (= (english-basic-of-string "Égalité")) '(59 (5 7 1 12 9 20 5)))
  (is (= (english-basic-of-string english-alphabet))
      '(351 (1 2 3 4 5 6 7 8 9 10
               11 12 13 14 15 16 17 18 19
               20 21 22 23 24 25 26))))

(deftest test-english-gematria-standard
  (is (= 1   (english-gematria-standard \A)))
  (is (= 20  (english-gematria-standard \k)))
  (is (= 90  (english-gematria-standard \r)))
  (is (= 100 (english-gematria-standard \S)))
  (is (= 800 (english-gematria-standard \Z)))
  (is (= 0   (english-gematria-standard \*))) ; test symbol
  (is (= 0   (english-gematria-standard \space)))
  (is (= 0   (english-gematria-standard \9)))
  (let [result-list '(1 2 3 4 5 6 7 8 9 
                        10 20 30 40 50 60 70 80 90 
                        100 200 300 400 500 600 700 800)]
    (dotimes [i 26]
      (is (= (english-gematria-standard (nth english-alphabet i))
             (nth result-list i))))
    (let [up-alphabet (str/upper-case english-alphabet)]
      (dotimes [i 26]
        (is (= (english-gematria-standard (nth up-alphabet i))
               (nth result-list i))))))
  (let [numbers "0123456789"] ; as strings due to type checking
    (dotimes [i 10]
      (is (= (english-gematria-standard (nth numbers i)) 0))))
  (let [symbols "!$%^-_*"]
    (dotimes [i 7]
      (is (= (english-gematria-standard (nth symbols i)) 0)))))

(deftest test-english-standard-of-string
  (is (= (english-standard-of-string "a")) '(1 (1)))
  (is (= (english-standard-of-string "Archie")) '(116 (1 90 3 8 9 5)))
  (is (= (english-standard-of-string "aR!_ chi$E"))
      '(116 (1 90 0 0 0 3 8 9 0 5)))
  (is (= (english-standard-of-string "Archie1")) '(117 (1 90 3 8 9 5 1)))
  (is (= (english-standard-of-string "Égalité")) '(257 (5 7 1 30 9 200 5)))
  (is (= (english-standard-of-string english-alphabet))
          '(4095 (1 2 3 4 5 6 7 8 9 10
                    20 30 40 50 60 70 80 90 100
                    200 300 400 500 600 700 800))))

(deftest test-greek-char?
  (is (= (greek-char? \Ͱ)))
  (is (= (greek-char? \ͩ)))
  (is (= (greek-char? \Δ)))
  (is (= (greek-char? \ΰ)))
  (is (= (greek-char? \ἇ)))
  (is (= (greek-char? \A)))
  (is (= (greek-char? \Α)))
  (is (= (greek-char? \α))))

(deftest test-greek-str?
  (is (= (greek-str? "Archie")  false))
  (is (= (greek-str? "Ιησούς")  true))
  (is (= (greek-str? "ιησους")  true))
  (is (= (greek-str? "Ιησούς!") false))
  (is (= (greek-str? "ᾦNOPE!")  false)))

(deftest test-normalized-greek-char?
  (is (= (normalized-greek-char? \Δ) false))
  (is (= (normalized-greek-char? \ΰ) false))
  (is (= (normalized-greek-char? \δ) true))
  (is (= (normalized-greek-char? \A) false))
  (is (= (normalized-greek-char? \Α) false))
  (is (= (normalized-greek-char? \α) true)))

  
;(set! *assert* true)
;(set! *assert* false)
(run-tests 'gematria.core-test)
