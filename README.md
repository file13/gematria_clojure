# gematria

This is a silly program in Clojure I used to experiment with Clojure.
It implements a full blown program to do basic gematria queries
for number and words in English, Ancient Greek, and Biblical Hebrew.

There's also a live web app version here:

[https://gematriaweb.herokuapp.com/](https://gematriaweb.herokuapp.com/)

and the repo is here:

[https://gitlab.com/file13/gematriaweb](https://gitlab.com/file13/gematriaweb)

## Installation

There's a fully functioning GUI desktop app along with a decent sized
database of words include here:

[gematria-desktop.zip](https://gitlab.com/file13/gematria/blob/master/resources/gematria-desktop.zip)

You'll need Java 8 or later since we use hikari-cp for pooling.

## Usage

In many environments, just unzip _gematria-desktop.zip_  and double click on the file:

    gematria-0.1.0-standalone.jar

Yes, it's Swing, so it's fugly, but hey, it's portable.

Otherwise you can launch it from the command line like so:

    $ java -jar gematria-0.1.0-standalone.jar


## Examples

Here's a few simple examples of the output generated.

#### A number query: 42

42: (ahlab babble bail balah baled bali blade bm deified diked edified elbe flea gel glad hike jigged lbj leaf leg mb)

42: (αμα βααλγαδ βαλδαδ γαλη δικη θααλα κακα)

42: (בגזל ואהל חדל הגדל בחבל ההבל הואל והאל טבאל בלי בכיי כבודי וכבדי לבי כיבי בלאט הלז ולו אהלו ויכו והכהו הודויהו לבדו יכבדו אלאדו לגגו היטיבו בלבבו בהלה גדלה ואלה הואהכה והודויה זהיהיה לזה כיזה אלוה בידיהוה בלהה להבה לדאבה ולאה חלד כיהגד ולבד ויכבד יוכבד לגוג בחלב ואחיטוב הלהב ובלבב לחבב לאחאב בטלא לבטא הלוא להוא כיהוא לחגא)



#### An English word query: Trump

Trump => 700 or |[700 (200 90 300 40 70)]|

(advert approved artists assureds calumniating castrates colfax critiqued deflower descriptions enumerated evert ewers firestorms flowered fluorite fritters goodwill guillotine housebroken infiltrates inoffensive jettisoning jowls lovebirds lusitania madwomen militaristic mouthpiece mows obstetrical optimistic palpitation paracelsus platinum plentiful pompous precocious preconceive prepossess remotest removes repulses restest resurfaces revilings rottener schopenhauer serves setters severs seward sewer silvering sleeplessness slivering sternness straits streets stresses sukkot terminates tersest testers trump tsunami tut untapped uv vaselines verses waders walloped wearier whimsical wimbledon winnipeg worn y)

700: (ακροατης αναιρεθησεται ανηκοντας απαντησιν αποκρυβηθι απτηται βηρυλλιον γενηθεντος γραμματεις γρηγορησης δακνοντες διεσκορπισα διηρπαζοντο εγκαταλειπομενοι εκδειραντες εκκλινουμεν εκλυομενοι εμπλακησεται εξιοντες επιταγμασιν μαδιανιτιδος μανασσης οικοδομησης ορνιου ουριον παρρησιας περιελου ποθεινοτερα πυρον ρυπον υπεμενον φοινιξ φονοι)

700: (שת קרת בחצרת כפרת מכמרת פכרת ממכרת רקת לרעת כירעת רננת ערלת פרכת רצית וחרפות ידמרמות עלשש כפתר שרר עלשקר תכפר תסמר ואלתסגר ששק לתרע תכריע תרמס ן קם לעם כיעם לכנם עלם נכלם לכלכם וידעים יודעים מנים ובלבנים מימים המאדמים מכלים בנחלים ובנבלים למכים מלכים במחליים סכיים לאדניהם מכלהאדם רך עצמך מקללך פסללך עליפיך מקניך פעמיך מנעליך פקודיך וממעמדך צרתי עכרתי ורדפתי יריעתי ערכתי תצרי שניסריסי רעתכי עלצדקתו וחרפתו רפידתו ותחפרו יתפרדו לאנותרהבו אשרצדקה עםיהודה אשרהפקיד)


#### A Greek word query: Ιησούς

Ιησούς => 888 or |[888 (10 8 200 70 400 200)]|

(αγνοησαντες ανακαυσεις ανταποθανειται αποκτειναντα αυταρκειαν βελσαττιμ γεισους δεδωκεν διεθρεψεν διεφθαρησαν διορυγμασιν ειρηνευετε εκβαλλω εκπορευση ελεγχομενοι εξουθενηκασιν επιβαινουσιν επιβαλλουσιν επληθυνατε εσχηκεν εχαλασαν ηλων ιησους καταρασσειν κατελειφθη κατεταξας κτιστην λαβωεμαθ λεβωνα λεγων ληνω λυπηρος μεμολυσμενη μερισθησεται οφειλησειν παρακληθησονται πεπαιδευμενης πληρους προσκαυθη στερηθεισαν συλλαβεσθαι συνθηκας σχοιη φαρμακειαις)

888: (adorably arboretums associative bullshitted constantinople depressives devotions equivalence equivocal evocations factitious ferociousness garry happily illuminations inaugurals jehezekel kidneys kwangju nationwide pawnbroker periwinkles phony precautions predetermination provident reformulating shortstop slangy spacesuits supplicates swindlers telecommute ulcerous uncompromising undisguised unstoppable unweighed woodiness)

888: (פחתת ובתפת נגעהצרעת פתחת לנתבות ביתעבדת חפף ובכסף ובעירם ויעברם וירבעם ועברים הארבעים לחרמים לרחמים המרגלים וכלברכים לרמחים ורעבים יעברום שלחנך ותתעבי תפתח אתטבעתו ותכונתו אתנבלתה בןאלקנה כנףהאחד)

#### A Hebrew word: יהוה

26 or |[26 (10 5 6 5)]|

(חחי אחזי גוזי וגזי בידי החגי הביט יאחז והבוז ויגז בחיו והטו בוזהו וידו יודו בחטאו אכה הויה והיה חגיה וחזה חוזה יהוה והדוה והגבהה ויגבה וידאה בכד בדויד חדיד וההוד ויבאגד כבד היטב כדב ויובב ובחטא דיהוא דידהבא)

26: (back bagged caddie cicadae deice efface fief fife haddah hadid hagia hide hied jibe jig)

26: (αγγιθ αιει ζαβαδια ιαδαι ιαδια)

### TODO

A web based version for the hell of it.

## License

Copyright © 2016 Michael Rossi

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
