(defproject gematria "0.1.0"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 ;; Database
                 [org.clojure/java.jdbc "0.6.1"]
                 [hikari-cp "1.7.5"]
                 [com.h2database/h2 "1.4.192"]
                 ;; Gui
                 [seesaw "1.4.5"]
                 ]
  :main ^:skip-aot gematria.gui
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
