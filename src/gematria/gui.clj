(ns gematria.gui
  (:require
   ;; General
   [clojure.pprint :refer :all]
   [clojure.string :as str]
   [gematria.core :refer :all]
   ;; Database
   [clojure.java.jdbc :as jdbc]
   [hikari-cp.core :as pool]
   ;; Gui
   [seesaw.core :refer :all]
   )
  (:gen-class))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  H2 database with HikariCP pool
;;; (use 'gematria.core :reload)
;;;
;;SELECT word FROM esd WHERE number = 24
;;UNION SELECT word from gsd WHERE number = 24
;;UNION SELECT word from hsd WHERE number = 24
;;ORDER BY word;
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def datasource-options
  {:adapter "h2"
   :username  "sa"
   :password ""
   :url     (str "jdbc:h2:" (System/getProperty "user.dir") "/resources/gematria")})

(def datasource
  (pool/make-datasource datasource-options))

(defn db-connection
  "Heler to access the database connection."
  []
  {:datasource datasource})

(defn make-dict
  [name db converter normalizer]
  {:name name :db db :converter converter :normalizer normalizer})

(def english
  (make-dict "English Standard" "esd" english-standard-of-string normalize-english))

(def greek
  (make-dict "Greek Standard" "gsd" greek-standard-of-string normalize-greek))

(def hebrew
  (make-dict "Hebrew Standard" "hsd" hebrew-standard-of-string normalize-hebrew))

(def all-dict [english greek hebrew])

(defn clob->string
  "Convert a clob into a string since H2 returns clobs."
  [clob]
  (with-open [rdr (java.io.BufferedReader. (.getCharacterStream clob))]
    (apply str (line-seq rdr))))

(defn sql-lookup-entry
  "Does the actual jdbc H2 SQL lookup."
  [dictionary num]
  (let [q (str "select word from " (:db dictionary) " where number = " num)]
    (jdbc/query (db-connection)
             [q]
             {:row-fn
              (comp clob->string :word)
              })))

(defn sql-lookup-num
  "Returns a string of the results of a number lookup."
  [dictionary num]
  (cl-format false "~A: ~A~%~%" num (sql-lookup-entry dictionary num)))

(defn sql-lookup-str
  "Returns a string of the results of a string (aka, word) lookup."
  [dictionary s]
  (let [results ((:converter dictionary) s)
        n (first results)]
    (list n (cl-format false "~A => ~A or |~A|~%~%~A~%~%" s n results
                       (sql-lookup-entry dictionary n)))))

(defn sql-lookup
  "Looks up either a number or a string in a dictionary."
  [dictionary num-or-string]
  (if (number? num-or-string)
    (sql-lookup-num dictionary num-or-string)
    (sql-lookup-str dictionary num-or-string)))

(defn sql-lookup-multiple-num
  "Looks up multiple number entries."
  [num ordered-dictionaries]
  (str/join
   (map #(sql-lookup-num % num) ordered-dictionaries)))

(defn sql-lookup-multiple-str
  "Looks up multiple string entries "
  [s ordered-dictionaries]
  (let [lst (sql-lookup-str (first ordered-dictionaries) s)]
    (str (second lst)
         (str/join
          (map #(sql-lookup-num % (first lst)) (rest ordered-dictionaries))))))


(defn sql-lookup-all
  "Looks up a num or string and returns a string lookup formatted correctly.
  Maybe we could do this as a multimethod?"
  [num-or-string]
  (if (parse-int num-or-string)
    (sql-lookup-multiple-num num-or-string [english greek hebrew])
    (cond
      (greek-str? num-or-string) (sql-lookup-multiple-str num-or-string
                                                          [greek english hebrew])
      (hebrew-str? num-or-string) (sql-lookup-multiple-str num-or-string
                                                           [hebrew english greek])
      :else (sql-lookup-multiple-str num-or-string [english greek hebrew]))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAIN
;;;
;;(use 'seesaw.dev)
;;(show-options (frame))
;;;
;;;  This would still need a lot of work to make nicer. Including
;;;  Scrollbars and such.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro in-flow
  "Helper to wrap a seesaw objects in a flow panel. 
  This is a Swing trick to stop resizing the layout."
  [& items]
  `(flow-panel :items [~@items]))

(defn -main
  "A Seesaw GUI App."
  [& args]
  (def main-frame
    (frame :title "Gematria"
           :minimum-size [425 :by 400]
           :resizable? true
           :on-close :exit
           ;;:on-close :hide
           ))
  (def input-label
    (in-flow
     (label :h-text-position :center
            :v-text-position :center
            :text "Enter Number or Word"
            )))

  (def input-text
    (in-flow
     (text :id :input-text
           :font "ARIAL-PLAIN-16"
           :border 5
           :preferred-size [400 :by 50]
           :text "Ιησούς")))

  (def compute-button
    (in-flow
     (button :id :compute-button
             :text "Compute"
             :valign :center
             :halign :center
             ;; :mnemonic ENTER
             )))
  (def output-text
    (text :id :output-text
          :multi-line? true
          :wrap-lines? true
          :font "ARIAL-PLAIN-20"
          :text
          (str
           "Please note, I understand English, Greek, and Hebrew unicode input."
           "\n\nYou can also hit Enter/Return to compute something.")))

  (def v-panel
    (vertical-panel
     :items [input-label
             input-text
             compute-button
             output-text]))

  (native!)
  (config! main-frame :content v-panel)
  (listen
   (select main-frame [:#compute-button])
   :action
   (fn [e]
     (value! output-text
             (sql-lookup-all (value (select main-frame [:#input-text]))))))

  (-> main-frame show!)
)
;;(-main)
